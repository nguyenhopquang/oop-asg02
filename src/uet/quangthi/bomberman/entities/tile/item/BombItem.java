package uet.quangthi.bomberman.entities.tile.item;

import uet.quangthi.bomberman.Game;
import uet.quangthi.bomberman.entities.Entity;
import uet.quangthi.bomberman.entities.character.Bomber;
import uet.quangthi.bomberman.graphics.Sprite;

public class BombItem extends Item {

	public BombItem(int x, int y, int level, Sprite sprite) {
		super(x, y, level,sprite);
	}

	@Override
	public boolean collide(Entity e) {
		if(e instanceof Bomber) {
			((Bomber) e).addPowerup(this);
			remove();
			return true;
		}

		return false;
	}
	@Override
	public void setValues() {
		_active = true;
		Game.addBombRate(1);
	}
	


}
