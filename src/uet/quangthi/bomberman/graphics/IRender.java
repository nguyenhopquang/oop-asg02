package uet.quangthi.bomberman.graphics;

public interface IRender {

	public void update();
	
	public void render(Screen screen);
}
