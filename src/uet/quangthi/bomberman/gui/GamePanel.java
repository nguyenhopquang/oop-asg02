package uet.quangthi.bomberman.gui;

import uet.quangthi.bomberman.Game;
import uet.quangthi.bomberman.exceptions.GameException;

import javax.swing.*;
import java.awt.*;

/**
 * Swing Panel chứa cảnh game
 */
public class GamePanel extends JPanel {

	private Game _game;

	public GamePanel(Frame frame) {
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(Game.WIDTH * Game.SCALE, Game.HEIGHT * Game.SCALE));

		try {
			_game = new Game(frame);

			add(_game);

			_game.setVisible(true);

		} catch (GameException e) {
			e.printStackTrace();
			//TODO: so we got a error hum..
			System.exit(0);
		}
		setVisible(true);
		setFocusable(true);

	}
	public void changeSize() {
		setPreferredSize(new Dimension(Game.WIDTH * Game.SCALE, Game.HEIGHT * Game.SCALE));
		revalidate();
		repaint();
	}

	public Game getGame() {
		return _game;
	}
	
}
