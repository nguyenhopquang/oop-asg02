package uet.quangthi.bomberman.level;

import uet.quangthi.bomberman.exceptions.LoadLevelException;

public interface ILevel {

    public void loadLevel(int level) throws LoadLevelException;
}
